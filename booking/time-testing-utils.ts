import dayjs, { Dayjs, OpUnitType } from 'dayjs'
import * as Timeslot from './timeslot'
const d = dayjs

export function ts(start: Dayjs, end: Dayjs): Timeslot.T {
  return {
    start: start.toDate(),
    end: end.toDate(),
  }
}

export function tsStream() {
  let prev: Dayjs
  return (value?: number, unit?: OpUnitType): Dayjs => {
    if (!prev) {
      prev = d()
      return prev
    } else if (value === undefined || unit === undefined) {
      return prev
    } else {
      prev = prev.add(value, unit)
      return prev
    }
  }
}

export function diff({ start, end }: Timeslot.T, unit: OpUnitType) {
  return d(end).diff(d(start), unit)
}
