import { object, string, date, validate } from 'superstruct'
import * as OptimizedTimeslot from './optimized-timeslot'

export type T = OptimizedTimeslot.T & {
  chefName: string
}

const schema = object({
  start: date(),
  end: date(),
  utcStartDay: date(),
  utcEndDay: date(),
  chefName: string(),
})

export function create(i: unknown) {
  return validate(i, schema)
}
