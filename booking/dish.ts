import {
  array,
  enums,
  object,
  string,
  validate,
  optional,
  Infer,
} from 'superstruct'

const schema = object({
  name: string(),
  description: optional(string()),
  meta: object({
    type: enums(['entree', 'side', 'dessert']),
    substitutions: optional(array(enums(['vegan', 'gluten free']))),
  }),
  id: optional(string()),
})

export type T = Infer<typeof schema>

export function create(i: unknown) {
  return validate(i, schema)
}
