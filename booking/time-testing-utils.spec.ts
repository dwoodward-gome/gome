import { tsStream, ts, diff } from './time-testing-utils'

describe('tsStream', () => {
  it('Should produce expected results', () => {
    let s = tsStream()
    let first = s()
    expect(diff(ts(first, s(30, 'm')), 'm')).toStrictEqual(30)
    expect(diff(ts(first, s(-20, 'm')), 'm')).toStrictEqual(10)
    expect(diff(ts(first, s(50, 'm')), 'm')).toStrictEqual(60)
  })
})
