import { create } from 'superstruct'
import {
  _timestampToDate,
  _numberFromString,
  _headCount,
  MAX_HEAD_COUNT,
  MIN_HEAD_COUNT,
} from './booking'

describe('_timestampToDate', () => {
  it('Should parse a Date from a timestamp string', () => {
    const input = new Date()
    const actual = create(input.valueOf().toString(), _timestampToDate)
    expect(actual).toBeInstanceOf(Date)
    expect(actual.valueOf()).toEqual(input.valueOf())
  })

  it('Should throw when given a bad timestamp', () => {
    expect(() => create('asdfasdf', _timestampToDate)).toThrow()
  })

  it('Should throw when given a different string format for a date', () => {
    expect(() => create(new Date().toUTCString(), _timestampToDate)).toThrow()
  })

  it('Should not work when given a UTC date', () => {
    const input = new Date()
    const actual = create(input.toISOString(), _timestampToDate)
    expect(actual).toBeInstanceOf(Date)
    expect(actual.valueOf()).not.toEqual(input.valueOf())
  })

  it('Should work when given an actual date', () => {
    const input = new Date()
    const actual = create(input, _timestampToDate)
    expect(actual).toBeInstanceOf(Date)
    expect(actual.valueOf()).toEqual(input.valueOf())
  })
})

describe('_numberFromString', () => {
  it('Should parse a number from a string', () => {
    const input = 123456789
    const actual = create(input.toString(), _numberFromString)
    expect(actual).toEqual(input)
  })

  it('Should throw when given a bad string', () => {
    expect(() => create('asdfasdf', _numberFromString)).toThrow()
  })
})

describe('_headCount', () => {
  it('Should throw when given a non number', () => {
    expect(() => create(null, _headCount)).toThrow()
    expect(() => create('', _headCount)).toThrow()
    expect(() => create(undefined, _headCount)).toThrow()
  })

  it('Should throw when given numbers outside the range', () => {
    expect(() => create(MIN_HEAD_COUNT - 1, _headCount)).toThrow()
    expect(() => create(MAX_HEAD_COUNT + 1, _headCount)).toThrow()
  })
})
