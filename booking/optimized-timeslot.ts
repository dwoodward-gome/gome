import * as Timeslot from './timeslot'

export type T = Timeslot.T & {
  utcStartDay: Date
  utcEndDay: Date
}

export const schema = ''
