import {
  enums,
  object,
  string,
  Infer,
  number,
  date,
  min,
  max,
  coerce,
  tuple,
} from 'superstruct'
import * as Cuisine from './cuisine'
import * as Location from './location'

export const SERVICE_DURATION_IN_MINUTES = 120
export const TIME_BETWEEN_SERVICES = 30

export const MIN_HEAD_COUNT = 1
export const MAX_HEAD_COUNT = 6

export function make(): Partial<T> {
  return {
    when: undefined,
    headCount: undefined,
    where: undefined,
    cuisine: undefined,
  }
}

export const _numberFromString = coerce(number(), string(), (s: string) =>
  parseInt(s, 10)
)

export const _timestampToDate = coerce(
  date(),
  string(),
  (s: string) => new Date(parseInt(s))
)

export const _headCount = min(
  max(_numberFromString, MAX_HEAD_COUNT),
  MIN_HEAD_COUNT
)

export const _where = tuple([
  enums(Location.values),
  min(
    max(_numberFromString, Location.MAX_APPT_NUMBER),
    Location.MIN_APPT_NUMBER
  ),
])

export const schemaFromHtml = object({
  when: _timestampToDate,
  headCount: _headCount,
  where: _where,
  cuisine: enums(Cuisine.values),
})

export type T = Infer<typeof schemaFromHtml>
