export type T = 'Domain @ Brewers Hill'
export const values: T[] = ['Domain @ Brewers Hill']

export const MIN_APPT_NUMBER = 0
export const MAX_APPT_NUMBER = 1000
