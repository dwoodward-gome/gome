export type T = 'french' | 'american' | 'italian'
export const values: T[] = ['french', 'american', 'italian']
