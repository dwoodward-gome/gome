import * as Dish from './dish'
import * as Cuisine from './cuisine'

export type T = Record<Cuisine.T, Dish.T[]>

export const menu: T = {
  french: [
    {
      id: 'AQmSVuWtX812t0p1mno5',
      name: 'French Grilled Romaine Hearts',
      description: '',
      meta: {
        type: 'side',
        substitutions: ['vegan', 'gluten free'],
      },
    },
    {
      id: 'CmsM6cqbLkS0iaTvn8P3',
      description:
        'Aerated baked egg custard topped with powdered sugar and creme anglaise',
      name: 'Chocolate Soufflé',
      meta: {
        type: 'dessert',
        substitutions: ['vegan'],
      },
    },
    {
      id: 'X7feG9R9gP4lEnVXzzkE',
      description: '',
      name: 'Lyonnaise Potatoes',
      meta: {
        type: 'side',
        substitutions: ['vegan', 'gluten free'],
      },
    },
    {
      id: 'i9ZSvFD3ChFolJ9Pf2Yj',
      description: 'thin dessert pancake with a flambe citrus and brance sauce',
      name: 'Crepe Suzette/Chocolate',
      meta: {
        type: 'dessert',
        substitutions: ['vegan'],
      },
    },
    {
      id: 'jWd4udZpK3mwheOLptvU',
      name: 'Seasonal Fruit Galette',
      description: 'Rustic free form tart with macerated fruit mixture',
      meta: {
        type: 'dessert',
        substitutions: ['vegan', 'gluten free'],
      },
    },
    {
      id: 'kN6DzPZs6h6q1J4rauCF',
      name: 'Coq Au Vin with buttered Potatoes',
      description:
        'Red wine braised chicken pieces stewed tender with root vegetables herbs and spices',
      meta: {
        type: 'entree',
        substitutions: ['vegan', 'gluten free'],
      },
    },
    {
      id: 'r6hmPV8aBiul3k6Ouykw',
      name: 'Nicoise Salad',
      description: '',
      meta: {
        type: 'side',
        substitutions: ['vegan', 'gluten free'],
      },
    },
    {
      id: 't0Z3x3G0XU3K1P2EdlzD',
      name: 'Bouillabaisse',
      description:
        'Seafood stew with tomato base white wine and stock cook with an assortment of vegetables',
      meta: {
        type: 'entree',
        substitutions: ['vegan', 'gluten free'],
      },
    },
    {
      id: 'zKPdaSG1bpNXigpf9TYx',
      name: 'Beef Bourguignon',
      description:
        'Beef stewed in a bold rich Demi-glacé sauce with herbs and vegetables',
      meta: {
        type: 'entree',
        substitutions: ['vegan', 'gluten free'],
      },
    },
    {
      id: 'zUN32PWvvnTmKIcQ6zam',
      description: '',
      name: 'Ratatouille',
      meta: {
        type: 'side',
        substitutions: ['vegan', 'gluten free'],
      },
    },
  ],
  american: [
    {
      id: 'BujeLNFgWZgt0AWUvOSq',
      name: 'Baked Potato',
      description: '',
      meta: {
        type: 'side',
        substitutions: ['vegan', 'gluten free'],
      },
    },
    {
      id: 'TLWnkzKGCTcyGjQAim8W',
      description: '',
      name: 'Mashed Potatoes',
      meta: {
        type: 'side',
        substitutions: ['vegan', 'gluten free'],
      },
    },
    {
      id: 'XGFVdar3jt6pNriFcCEo',
      name: 'Rice Pilaf',
      description: '',
      meta: {
        type: 'side',
        substitutions: ['vegan', 'gluten free'],
      },
    },
    {
      id: 'Xq3vHwzk4W82r3g9HQvU',
      description:
        'Layered with citrus fruits, herbs and spices and slowly cooked to opaque doneness finished with buttered pan sauce',
      name: 'Whole Roasted Fish with blistered tomatoes and potatoes',
      meta: {
        type: 'entree',
        substitutions: ['gluten free'],
      },
    },
    {
      id: 'Z87uzOePiTH48CQX6ohy',
      description:
        'Large cut of choice pan seared and basted to desired doneness with herbs and aromatics',
      name:
        'Tomahawk Steak/Chop Roasted Asparagus with Parmesan and Mashed Potatoes',
      meta: {
        type: 'entree',
        substitutions: ['gluten free'],
      },
    },
    {
      id: 'hrWJfryi8ktmyehGeLCu',
      description:
        'Whole Roasted Chicken severed at the spine pan seared and roasted golden brown with a medley of herbs and vegetables',
      name: 'Spatchcock Whole Roasted Chicken and Seasonal Vegetables',
      meta: {
        type: 'entree',
        substitutions: ['gluten free'],
      },
    },
    {
      id: 'nOnKyaTBoopy926PyCHJ',
      description: '',
      name: 'Herb Roasted Potatoes',
      meta: {
        type: 'side',
        substitutions: ['vegan', 'gluten free'],
      },
    },
    {
      id: 'nS4O6X1NW9mlq4jtyEZJ',
      description: 'Finished with berry reduction and whipped cream',
      name: 'NY Style Cheesecake with Macerated Berry Compote',
      meta: {
        type: 'dessert',
        substitutions: ['vegan'],
      },
    },
    {
      id: 'rYrrabEGHYEdj2Ya06oj',
      description:
        'Mildly warm slice topped with ice cream selection of choice',
      name: 'Apple Pie a la mode',
      meta: {
        type: 'dessert',
        substitutions: ['vegan'],
      },
    },
    {
      id: 'tOaNP7EETtiJnQiK93N9',
      description:
        'Flambé then drizzled table side with Vanilla Bean Ice Cream',
      name: 'Bananas Foster with Bourbon Caramel',
      meta: {
        type: 'dessert',
        substitutions: ['vegan', 'gluten free'],
      },
    },
    {
      id: 'wQEefEV1Y0J2MTFXpV8w',
      description: '',
      name: 'Steamed Broccoli',
      meta: {
        type: 'side',
        substitutions: ['vegan', 'gluten free'],
      },
    },
    {
      id: 'wR6dHzmJN7sOFokuRvDU',
      name: 'Roasted Brussel Sprouts',
      description: '',
      meta: {
        type: 'side',
        substitutions: ['vegan', 'gluten free'],
      },
    },
  ],
  italian: [
    {
      id: '6kS7I7QtcQhXWWX1ABl7',
      name: 'Prosciutto Wrapped Asparagus',
      description: '',
      meta: {
        type: 'side',
        substitutions: ['gluten free'],
      },
    },
    {
      id: 'CUU14KL0LPJ0U5ptVeAl',
      description: '',
      name: 'Garlic Bread',
      meta: {
        type: 'side',
        substitutions: ['vegan'],
      },
    },
    {
      id: 'PLLfWha5qDq2rnLBXlpp',
      description:
        'chilled vanilla custard accompanied by seasonal fruit puree',
      name: 'Seasonal Panna Cotta',
      meta: {
        type: 'dessert',
        substitutions: ['gluten free'],
      },
    },
    {
      id: 'WyA5zIobi3a92saZ9Mtb',
      name: 'Arancini',
      description: '',
      meta: {
        type: 'side',
        substitutions: ['vegan'],
      },
    },
    {
      id: 'Yv08gTUcSX94chegXp4z',
      description: '',
      name: 'Italian Salad',
      meta: {
        type: 'side',
        substitutions: ['vegan'],
      },
    },
    {
      id: 'bFLBkShRasx4bIBb2Dwt',
      name: 'Pasta Carbonara with Pancetta, Parmesan and Egg Yolk',
      description:
        'Lightly tossed pasta with fresh cracked pepper, egg yolks and Parmesan to a velvety consistency',
      meta: {
        type: 'entree',
        substitutions: ['vegan', 'gluten free'],
      },
    },
    {
      id: 'f9ZAzgTJdL9FqnNX4yAa',
      description:
        'Freshly marinated seafood medley prepared in a light tomato sauce with fresh basil and garlic',
      name: 'Seafood Pasta with Basil Garlic and Capers',
      meta: {
        type: 'entree',
        substitutions: ['gluten free'],
      },
    },
    {
      id: 'hLCzjZ2M2Q9kJOkFNwpe',
      description:
        'Crispy tubular shell piped with sweetened cheese paired with nuts or chocolate nibs',
      name: 'Sicilian Cannoli',
      meta: {
        type: 'dessert',
        substitutions: ['gluten free'],
      },
    },
    {
      id: 'xIzMV9WKkzAwrr5Xj4WJ',
      name: 'Angel Cake Berry Trifle',
      description:
        'Sponge cake prepared and layered with fresh fruit, custard, jellies and cream',
      meta: {
        type: 'dessert',
        substitutions: ['vegan', 'gluten free'],
      },
    },
  ],
}
