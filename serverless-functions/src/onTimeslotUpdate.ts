import * as admin from 'firebase-admin'
import * as functions from 'firebase-functions'
import dayjs from 'dayjs'
import { Infrastructure } from '@gome/booking'
import { calculateUtcDay } from './helpers/timeslot-lookup'

export const onTimeslotUpdate = functions
  .region(Infrastructure.FUNCTIONS_REGION)
  .firestore.document('chef-timeslots/{slotId}')
  .onWrite(async (change) => {
    if (change.after.exists) {
      const prev = change.before.data()
      const next = change.after.data()

      const prevStart = prev?.start?.toDate()
      const nextStart = next?.start?.toDate()
      const prevEnd = prev?.end?.toDate()
      const nextEnd = next?.end?.toDate()

      if (
        prevStart?.valueOf() === nextStart?.valueOf() &&
        prevEnd?.valueOf() === nextEnd?.valueOf()
      ) {
        return Promise.resolve()
      }

      const res = {
        ...next,
      }

      if (nextStart) {
        res.utcStartDay = admin.firestore.Timestamp.fromDate(
          calculateUtcDay(dayjs(nextStart)).toDate()
        )
      }

      if (nextEnd) {
        res.utcEndDay = admin.firestore.Timestamp.fromDate(
          calculateUtcDay(dayjs(nextEnd)).toDate()
        )
      }

      return await change.after.ref.set(res)
    }
  })
