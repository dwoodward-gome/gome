import * as admin from 'firebase-admin'
import * as cert from '../admin-sdk-secrets.json'
const adminCredentials = cert as admin.ServiceAccount
admin.initializeApp({
  credential: admin.credential.cert(adminCredentials),
  databaseURL: 'https://gome-b271b.firebaseio.com',
})
export const firestore = admin.firestore()
