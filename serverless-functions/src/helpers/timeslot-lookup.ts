import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'
dayjs.extend(utc)
import { Booking, OptimizedTimeslot } from '@gome/booking'

const { SERVICE_DURATION_IN_MINUTES, TIME_BETWEEN_SERVICES } = Booking

export function calculateUtcDay(d: dayjs.Dayjs): dayjs.Dayjs {
  return d.utc().startOf('day')
}

export function _reserveTimeslot<S extends OptimizedTimeslot.T>(
  oldTimeslot: S,
  _serviceStartTime: Date
): { before?: S; after?: S } {
  assert(
    _serviceStartTime >= oldTimeslot.start &&
      _serviceStartTime <= oldTimeslot.end
  )

  const oldStart = dayjs(oldTimeslot.start)
  const oldEnd = dayjs(oldTimeslot.end)
  const serviceStartTime = dayjs(_serviceStartTime)
  const serviceEndTime = serviceStartTime.add(
    SERVICE_DURATION_IN_MINUTES,
    'minute'
  )

  const result = {
    before: null,
    after: null,
  }

  const thereIsEnoughTimeBeforeForAnotherTimeslot =
    serviceStartTime.diff(oldStart, 'minute') >=
    SERVICE_DURATION_IN_MINUTES + TIME_BETWEEN_SERVICES
  if (thereIsEnoughTimeBeforeForAnotherTimeslot) {
    const end = serviceStartTime.subtract(TIME_BETWEEN_SERVICES, 'minute')
    const tsBefore: S = {
      ...oldTimeslot,
      end: end.toDate(),
      utcEndDay: calculateUtcDay(end).toDate(),
    }
    result.before = tsBefore
  }

  const thereIsEnoughTimeAfterForAnotherTimeslot =
    oldEnd.diff(serviceEndTime, 'minute') >=
    TIME_BETWEEN_SERVICES + SERVICE_DURATION_IN_MINUTES
  if (thereIsEnoughTimeAfterForAnotherTimeslot) {
    const start = serviceEndTime.add(TIME_BETWEEN_SERVICES, 'minute')
    const tsBefore: S = {
      ...oldTimeslot,
      start: start.toDate(),
      utcStartDay: calculateUtcDay(start).toDate(),
    }
    result.after = tsBefore
  }

  return result
}

function assert(b) {
  if (!b) {
    throw new Error('')
  }
}
