import { Booking } from '@gome/booking'
import { ts, tsStream } from '@gome/booking/time-testing-utils'
import { _reserveTimeslot } from './timeslot-lookup'
import dayjs from 'dayjs'

const { SERVICE_DURATION_IN_MINUTES, TIME_BETWEEN_SERVICES } = Booking

describe('_reserveTimeslot', () => {
  it('Should return no sub-slots when there is no space on either side', () => {
    const s = tsStream()
    const noExtraSpace: any = ts(s(), s(SERVICE_DURATION_IN_MINUTES, 'm'))
    const { before, after } = _reserveTimeslot(noExtraSpace, noExtraSpace.start)
    expect(before).toBeFalsy()
    expect(after).toBeFalsy()
  })

  it('Should return a left sub-slot when there is just enough space before', () => {
    const s = tsStream()
    const slot: any = ts(
      s(),
      s(2 * SERVICE_DURATION_IN_MINUTES + TIME_BETWEEN_SERVICES, 'm')
    )
    const { before, after } = _reserveTimeslot(
      slot,
      s(-1 * SERVICE_DURATION_IN_MINUTES, 'm').toDate()
    )
    expect(before).toBeTruthy()
    expect(after).toBeFalsy()
  })

  it('Should return a right sub-slot when there is just enough space after', () => {
    const s = tsStream()
    const slot: any = ts(
      s(),
      s(2 * SERVICE_DURATION_IN_MINUTES + TIME_BETWEEN_SERVICES, 'm')
    )
    const { before, after } = _reserveTimeslot(slot, slot.start)
    expect(before).toBeFalsy()
    expect(after).toBeTruthy()
  })

  it('Should return a sub-slot that is TIME_BETWEEN_SERVICES apart', () => {
    const s = tsStream()
    const slot: any = ts(
      s(),
      s(2 * SERVICE_DURATION_IN_MINUTES + TIME_BETWEEN_SERVICES, 'm')
    )
    const dServiceStart = s(-1 * SERVICE_DURATION_IN_MINUTES, 'm')
    const { before } = _reserveTimeslot(slot, dServiceStart.toDate())
    const dBeforeEnd = dayjs(before.end)
    expect(dServiceStart.diff(dBeforeEnd, 'm')).toStrictEqual(
      TIME_BETWEEN_SERVICES
    )
  })
})
