// dayjs.extend(dayjs_plugin_utc)

const emptyTimeOpt = document.createElement('option')
emptyTimeOpt.value = ''
emptyTimeOpt.text = 'any time'

window.onload = function onLoad() {
  const dateInput = document.getElementById('date')
  const timeSelect = document.getElementById('when')

  let when = 0

  const datePicker = flatpickr(dateInput, {
    altInput: true,
    altFormat: 'M j, Y',
    disableMobile: true,
  })

  fetch('/tryit/__timestamp')
    .then((res) => res.json())
    .then((res) => {
      if (res && !isNaN(res) && typeof res === 'number') {
        when = res
        const TRIGGER_CHANGE_EVENT = true
        datePicker.setDate(
          dayjs(when).startOf('day').toDate(),
          TRIGGER_CHANGE_EVENT
        )
      }
    })

  dateInput.addEventListener('change', () => {
    // TODO actually put in the availibility
    const start = dayjs(dateInput.value).startOf('day')
    const availableTimes = [
      start.hour('18').minute(30),
      start.hour('18').minute(45),
      start.hour('19').minute(0),
      start.hour('19').minute(15),
      start.hour('19').minute(30),
    ]

    timeSelect.innerHTML = ''
    timeSelect.disabled = false

    const opts = availableTimes.map((t) => {
      const opt = document.createElement('option')
      opt.value = t.valueOf().toString()
      opt.text = t.format('h:mm a')
      return opt
    })

    opts.unshift(emptyTimeOpt)

    opts.forEach((opt) => {
      timeSelect.appendChild(opt)
    })

    if (when) {
      if (opts.some((o) => o.value === when.toString())) {
        timeSelect.value = when.toString()
        // will prevent future overrides
        when = 0
      }
    }
  })
}
