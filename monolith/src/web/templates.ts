import escapeHtml from 'escape-html'

// TODO reference this when moving on to inserting data into javascript: https://adamj.eu/tech/2020/02/18/safely-including-data-for-javascript-in-a-django-template/
// TODO here is a JS rather than JSON based approach: http://www.carlosramireziii.com/when-to-use-escape-javascript-in-an-sjr-template.html
// https://blog.presidentbeef.com/blog/2020/05/12/why-escape-javascript-is-dangerous/
// https://blog.presidentbeef.com/blog/2020/01/14/injection-prevention-sanitizing-vs-escaping/

export type EscapedString = _EscapedString

class _EscapedString {
  constructor(public value: string) {}
  toString(): string {
    return this.value
  }
}

function renderLeaf(leaf: _EscapedString | any): string {
  if (leaf instanceof _EscapedString) {
    return leaf.value
  } else {
    return escapeHtml('' + leaf)
  }
}

export function html(
  outer: TemplateStringsArray,
  ...inner: _EscapedString[] | any[]
): EscapedString {
  let result = ''
  let outerIndex = 0
  let innerIndex = 0
  while (outerIndex < outer.length || innerIndex < inner.length) {
    if (outerIndex < outer.length) {
      result += outer[outerIndex]
      outerIndex += 1
    }
    if (innerIndex < inner.length) {
      let cur = inner[innerIndex]
      if (!(cur instanceof Array)) {
        cur = [cur]
      }
      result += cur.map(renderLeaf).join('\n')
      innerIndex += 1
    }
  }
  return new _EscapedString(result)
}
