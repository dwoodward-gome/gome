export type T<Step, State> = {
  readonly steps: Step[]
  currentStep: Step
  state: State
}

export function make<Step, State>(
  args: Pick<T<Step, State>, 'steps' | 'state'>
): T<Step, State> {
  if (args.steps.length === 0) {
    throw new Error('')
  }

  return {
    ...args,
    currentStep: args.steps[0],
  }
}

export type NextResult<Step, State, Err> =
  | T<Step, State>
  | { result: T<Step, State>['state'] }
  | { error: Err }

export function goToNext<Step, State, Err>(
  validator: (
    currentStep: T<Step, State>['currentStep'],
    state: unknown
  ) => Err | undefined,
  { steps, currentStep }: T<Step, State>,
  newState: unknown
): NextResult<Step, State, Err> {
  const error = validator(currentStep, newState)
  const thereIsAnError = !!error
  if (thereIsAnError) {
    return { error }
  }

  // TODO this is a bit of a hack and assumes how the validator contract works
  // we many want to enforce this using the validator contract types down the line
  const checkedNewState = <State>newState

  const currentStepIndex = steps.indexOf(currentStep)
  const thisIsTheLastStep = currentStepIndex === steps.length - 1
  if (thisIsTheLastStep) {
    return { result: checkedNewState }
  } else {
    const theNextStep = steps[currentStepIndex + 1]
    return {
      steps,
      state: checkedNewState,
      currentStep: theNextStep,
    }
  }
}

export function goTo<Step, State>(
  step: T<Step, State>['currentStep'],
  state: T<Step, State>
): T<Step, State> {
  const { currentStep, steps } = state
  const desiredStepIndex = steps.indexOf(step)
  const currentStepIndex = steps.indexOf(currentStep)
  if (desiredStepIndex >= currentStepIndex) {
    return state
  } else {
    return {
      ...state,
      currentStep: step,
    }
  }
}
