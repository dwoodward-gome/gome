import { html } from './templates'

// TODO reference this when moving on to inserting data into javascript: https://adamj.eu/tech/2020/02/18/safely-including-data-for-javascript-in-a-django-template/

describe('html', () => {
  // I'm realizing this is going to get complicated when it comes to trying to escape css and or javascript as well...
  it('Should escape an interpolated script tag', () => {
    const badStr = '<script>window.alert("spookey")</script>'
    const actual = html`${badStr}`.toString()
    expect(actual).toEqual(expect.not.stringContaining('<script>'))
    expect(actual).toEqual(expect.not.stringContaining('</script>'))
  })

  it('Should escape interpolated script tags within an array', () => {
    const badStr = '<script>window.alert("spookey")</script>'
    const actual = html`${[1, 2, 3].map(() => badStr)}`.toString()
    expect(actual).toEqual(expect.not.stringContaining('<script>'))
    expect(actual).toEqual(expect.not.stringContaining('</script>'))
  })

  it('Should not rely on the default Array.toString method when interpolating arrays', () => {
    const actual = html`${[1, 2, 3].map((n) => n)}`.toString()
    expect(actual).toEqual(expect.not.stringContaining(','))
  })

  it('Should be able to take arrays as interpolated values', () => {
    const expected = '1\n2\n3'
    const actual = html`${[1, 2, 3].map((n) => n)}`.toString()
    expect(actual).toEqual(expected)
  })
})
