import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc'
dayjs.extend(utc)
import { Booking } from '@gome/booking'
import assert from '../../assert'
import { TryItPageLayout } from '../try-it-page-layout.view'
import { html } from '../../web/templates'

export function pluralizeGuests(num: number): string {
  return num === 1 ? 'guest' : 'guests'
}

function range(min: number, max: number): number[] {
  assert(min < max)
  const res = []
  for (let i = min; i <= max; i += 1) {
    res.push(i)
  }
  return res
}

const headCountRange = range(Booking.MIN_HEAD_COUNT, Booking.MAX_HEAD_COUNT)

export const TryItReservationPage: ReturnType<
  typeof TryItPageLayout
> = TryItPageLayout({
  heading: 'Book a chef',
  formButtonText: 'Fill in details',
  formFields: ({ wizard }) => html`
    <section class="mygrid">
      <span>I would like to make a reservation for</span>
      <select class="input" name="headCount" id="headCount" required>
        <option value="">any number of guests</option>
        ${headCountRange.map(
          (count) =>
            html`<option
              value="${count}"
              ${wizard.state.headCount === count ? 'selected' : ''}
            >
              ${count} ${pluralizeGuests(count)}
            </option>`
        )}
      </select>
      <span>on</span>
      <input
        type="text"
        placeholder="any date"
        class="input md:w-48"
        name="date"
        id="date"
        value=""
        required
      />
      <span>at</span>
      <select class="input" name="when" id="when" required>
        <option disabled selected value="">any time</option>
      </select>
      <span>in</span>
      <select class="input" name="mycity" required>
        <option value="">any city</option>
        <option value="bmore">Baltimore</option>
        <option value="dc">D.C.</option>
      </select>
      <span>.</span>
    </section>
  `,
  scriptOverrides: [
    'https://cdn.jsdelivr.net/npm/flatpickr',
    'https://cdn.jsdelivr.net/npm/dayjs',
    '/try-it-reservation.js',
  ],
  stylesheetOverrides: {
    external: ['https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css'],
  },
})
