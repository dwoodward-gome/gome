import { Cuisine, Location } from '@gome/booking'
import { TryItPageLayout } from '../try-it-page-layout.view'
import { html } from '../../web/templates'

export function capitalize(str: string) {
  return str && str.charAt(0).toUpperCase() + str.slice(1)
}

export const TryItDetailsPage: ReturnType<
  typeof TryItPageLayout
> = TryItPageLayout({
  heading: 'Book a chef',
  formButtonText: 'Checkout',
  formFields: ({ wizard }) => html`
    <section class="mygrid">
      <span>I would like to have my Gomë chef come to</span>
      <select class="input" name="where" required>
        <option value="">select a location</option>
        ${Location.values.map(
          (complex) => html`<option
            value="${complex}"
            ${wizard.state.where?.[0] === complex ? 'selected' : ''}
          >
            ${complex}
          </option>`
        )}
      </select>
      <span>appartment number</span>
      <input
        class="input"
        type="number"
        name="where"
        style="max-width: 120px"
        inputmode="numeric"
        min="${Location.MIN_APPT_NUMBER}"
        max="${Location.MAX_APPT_NUMBER}"
        required
        value="${wizard.state.where?.[1] || ''}"
      />
      <span>and prepare</span>
      <select class="input" name="cuisine" id="cuisine" required>
        <option value="">any</option>
        ${Cuisine.values.map(
          (c) =>
            html`<option
              value="${c}"
              ${wizard.state.cuisine === c ? 'selected' : ''}
            >
              ${capitalize(c)}
            </option>`
        )}
      </select>
      <span>cuisine.</span>
    </section>
  `,
  scriptOverrides: [],
  stylesheetOverrides: {},
})
