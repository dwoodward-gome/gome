import dayjs from 'dayjs'
import { TryItPageLayout } from '../try-it-page-layout.view'
import { html } from '../../web/templates'
import { capitalize } from './try-it-details.view'
import { pluralizeGuests } from './try-it-reservation.view'

function row(col1, col2) {
  return html`<section class="flex flex-col">
    <div class="text-gray-500">${col1}</div>
    <div>${col2}</div>
  </section>`
}

export const TryItCheckoutPage: ReturnType<
  typeof TryItPageLayout
> = TryItPageLayout({
  heading: 'Checkout',
  formButtonText: 'Book my chef',
  formFields: ({
    wizard: {
      state: { cuisine, headCount, when, where },
    },
    payment,
  }) => html`
    <section class="space-y-2 sm:space-y-4">
      ${row('Where', html`${where?.[0]} #${where?.[1]}`)}
      ${row('When', dayjs(when).format('h:mm A dddd, MMMM D, YYYY'))}
      ${row('Cuisine', html`${capitalize(cuisine)}`)}
      ${row(
        'Total',
        html`$70 &times; ${headCount} ${pluralizeGuests(headCount)} &#61;
        $${70 * headCount}`
      )}
    </section>

    <section class="space-y-4 sm:space-y-5">
      <h3 class="text-3xl">Payment method</h2>

      <div
        id="payment-card-element"
        class="ml-auto mr-auto py-3"
        style="max-width: 380px; border: none; border-bottom: dashed 2px black;"
      ></div>

      <div id="card-errors" role="alert" class="text-red-700 text-xl py-2"></div>

      <!-- FOR DATA PASSING ONLY -->
      <input
        type="hidden"
        id="client-secret"
        name="clientSecret"
        value="${payment.clientSecret || ''}"
      />
    </section>
  `,
  scriptOverrides: ['https://js.stripe.com/v3/', '/try-it-checkout.js'],
  stylesheetOverrides: {},
})
