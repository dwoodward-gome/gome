import { html } from '../web/templates'
import { MainLayout } from '../main-layout.view'
import * as BookingWizard from '../booking-wizard/booking-wizard'

export const TRY_IT_ROUTE = 'tryit'

function WizardNavCircle({
  index,
  checked,
  active,
}: {
  index: number
  checked: boolean
  active: boolean
}) {
  return html`
    <svg class="circle" viewBox="0 0 52 52" xmlns="http://www.w3.org/2000/svg">
      <!-- TODO this is a hack -->
      <a href="${`/${TRY_IT_ROUTE}/${index - 1}`}" id="circle-link-${index}">
        <circle
          cx="26"
          cy="26"
          r="23"
          stroke="black"
          fill="${active || checked ? 'black' : 'white'}"
          stroke-width="2"
        />
        ${checked
          ? html`<path
              stroke="white"
              stroke-width="2"
              fill="none"
              d="
              M 14.1 27.2
              l  7.1  7.2 16.7 -16.8
            "
            />`
          : html`<text
              x="50%"
              y="50%"
              text-anchor="middle"
              stroke="${active ? 'white' : 'black'}"
              fill="${active ? 'white' : 'black'}"
              class="text-xl"
              dy=".35em"
            >
              ${index}
            </text>`}
      </a>
    </svg>
    <!-- hacky styeles -->
    <label
      class="absolute top-12 text-sm sm:top-14 sm:text-base"
      style="left: 50%;"
      for="circle-link-${index}"
    >
      <div class="relative" style="left: -50%;">
        ${index === 1 ? 'Reservation' : index === 2 ? 'Details' : 'Checkout'}
      </div>
    </label>
  `
}

export function TryItPageLayout({
  heading,
  formFields,
  formButtonText,
  scriptOverrides = [],
  stylesheetOverrides: { internal = [], external = [] } = {},
}: {
  heading: any
  formFields: (s: BookingWizard.T) => any
  formButtonText: any
  scriptOverrides: any[]
  stylesheetOverrides: Partial<{ internal: any[]; external: any[] }>
}) {
  return function TryItPage(
    state: BookingWizard.T
  ): ReturnType<typeof MainLayout> {
    return MainLayout(
      {
        stylesheets: {
          internal: ['try-it.css', ...internal],
          external: [...external],
        },
        scripts: [...scriptOverrides],
      },
      html`
        <div class="py-10 sm:py-16 space-y-6 sm:space-y-12">
          <h1 class="text-center text-5xl font-semibold">${heading}</h1>
          <!-- hacky styeles -->
          <form
            class="my-10 text-2xl space-y-10 leading-loose px-2 sm:w-full sm:text-center ml-auto mr-auto"
            style="max-width: 910px;"
            id="booking"
            method="POST"
            action="${`/${TRY_IT_ROUTE}/${state.wizard.currentStep}`}"
          >
            ${formFields(state)}

            <div class="w-full">
              <button
                type="submit"
                id="booking-submit"
                class="ml-auto mr-auto block border-solid border-black border-2 text-2xl p-4"
              >
                ${formButtonText}
              </button>
            </div>
          </form>

          <!-- hacky styeles -->
          <nav
            class="sticky bg-white bottom-0 py-6 sm:py-0 text-xl wizard pl-4 pr-4 sm:ml-auto sm:mr-auto"
            style="max-width: 580px;"
          >
            <ul class="flex justify-around">
              ${BookingWizard.steps.map(
                (s, index) =>
                  html`
                    ${index
                      ? html`<li
                          aria-hidden="true"
                          class="connector mx-2"
                        ></li>`
                      : ''}
                    <li class="relative">
                      ${WizardNavCircle({
                        index: index + 1,
                        checked: BookingWizard.stepLt(
                          s,
                          state.wizard.currentStep
                        ),
                        active: index === state.wizard.currentStep,
                      })}
                    </li>
                  `
              )}
            </ul>
          </nav>
        </div>
      `
    )
  }
}
