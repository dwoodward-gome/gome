import { MainLayout } from '../main-layout.view'
import * as BookingWizard from '../booking-wizard/booking-wizard'
import { TryItReservationPage } from './steps/try-it-reservation.view'
import { TryItDetailsPage } from './steps/try-it-details.view'
import { TryItCheckoutPage } from './steps/try-it-checkout.view'

const pages = {
  [BookingWizard.Step.RESERVATION]: TryItReservationPage,
  [BookingWizard.Step.DETAILS]: TryItDetailsPage,
  [BookingWizard.Step.CHECKOUT]: TryItCheckoutPage,
}

export function TryItPage(
  state: BookingWizard.T
): ReturnType<typeof MainLayout> {
  const handler = pages[state.wizard.currentStep]
  if (!handler) {
    throw new Error('Unknown step: ' + state.wizard.currentStep)
  }
  return handler(state)
}
