import cn from 'classnames'
import { Dish, Cuisine } from '@gome/booking'
import { html } from './web/templates'
import { MainLayout } from './main-layout.view'

export const MENU_ROUTE = 'ourmenu'

export function prop<T, K extends keyof T>(field: K) {
  return (obj: T) => obj[field]
}

export function groupBy<T>(
  groupFn: (x: T) => string, // gets the group key
  vals: T[]
): { [k: string]: T[] } {
  return vals.reduce((prev, cur: T) => {
    const result = groupFn(cur)
    if (prev[result]) {
      prev[result].push(cur)
      return prev
    } else {
      return Object.assign(prev, { [result]: [cur] })
    }
  }, {})
}

function menuHeader(selected: Cuisine.T) {
  return html`
    <h1 class="text-center text-5xl font-semibold">Our Menu</h1>
    <section class="space-y-3 sm:space-y-8">
      <h2 class="text-center text-2xl">Cuisines</h2>
      <nav id="menunav">
        <ul class="flex flex-row justify-around">
          ${Cuisine.values.map(
            (c) =>
              html`<li>
                <a
                  href="/${MENU_ROUTE}/${c}"
                  class="${cn({ selected: c === selected })}"
                  >${c}</a
                >
              </li>`
          )}
        </ul>
      </nav>
    </section>
  `
}

const substitutionAbbreviations: Record<
  Dish.T['meta']['substitutions'][0],
  string
> = {
  vegan: 'V',
  'gluten free': 'GF',
}

function renderDish({ name, meta }: Dish.T) {
  const hasSubstitutions = meta.substitutions.length > 0
  const substitutions = hasSubstitutions
    ? html`
        -
        <span class="substitution">
          (${meta.substitutions
            .map((s) => substitutionAbbreviations[s])
            .join(') (')})
        </span>
      `
    : ''
  return html`${name}${substitutions}`
}

export function MenuPage(
  c: Cuisine.T,
  data: Dish.T[]
): ReturnType<typeof MainLayout> {
  const groupedCuisine = groupBy((d) => d.meta.type, data)
  return MainLayout(
    { stylesheets: { internal: ['menu.css'] } },
    html`
      <div class="py-10 sm:py-16 space-y-6 sm:space-y-12">
        ${menuHeader(c)}
        <section class="space-y-1 text-sm  sm:w-full sm:text-center">
          <h4 class="text-base">Legend:</h4>
          <p>(V) - vegan substitutions available</p>
          <p>(GF) - gluten free substitutions available</p>
          <p>Other substitutions may be available upon request.</p>
        </section>
        ${['entree', 'side', 'dessert'].map(
          (dishType) => html`
            <section class="menu space-y-1 sm:w-full sm:text-center">
              <h3 class="heading">${dishType}s</h3>
              <ul class="space-y-2">
                ${groupedCuisine[dishType].map(
                  (dish) => html` <li>${renderDish(dish)}</li> `
                )}
              </ul>
            </section>
          `
        )}
      </div>
    `
  )
}
