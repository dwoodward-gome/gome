import { Stripe } from 'stripe'
import assert from './assert'

const stripe = new Stripe('sk_test_nxPJP6pENZ8350XEkdMTLwOF00VSzBcmsW', {
  apiVersion: '2020-08-27',
})

export async function fetchPaymentIntentForBooking(headCount: number) {
  assert(!isNaN(headCount))
  assert(headCount > 0 && headCount < 6)

  const paymentIntent = await stripe.paymentIntents.create({
    amount: headCount * 7000,
    currency: 'usd',
  })

  return paymentIntent.client_secret
}
