import { _parseBookingForm } from './try-it.controller'

describe('Parse the form', () => {
  it('Should parse some things and not others', () => {
    const actual = _parseBookingForm({
      cuisine: 'french',
      date: '2020-12-24',
      when: '1608852600000',
      headCount: '2',
    })
    expect(actual).toEqual({
      when: new Date(1608852600000),
      cuisine: 'french',
      headCount: 2,
      where: undefined,
    })
  })
})
