import {
  Controller,
  Get,
  Header,
  Param,
  Req,
  Body,
  Post,
  Res,
} from '@nestjs/common'
import { create, partial, masked } from 'superstruct'
import { FastifyRequest, FastifyReply } from 'fastify'
import * as Wizards from './web/wizards'
import * as BookingWizard from './booking-wizard/booking-wizard'
import { TryItPage, TRY_IT_ROUTE } from './try-it-views'
import { Booking } from '@gome/booking'
import { fetchPaymentIntentForBooking } from './stripe-checkout'

const _bookingFormSchema = masked(partial(Booking.schemaFromHtml))
export function _parseBookingForm(input: unknown) {
  return create(input, _bookingFormSchema)
}

const _defaultStep = BookingWizard.Step.RESERVATION
function parseStep(s: string | undefined): BookingWizard.Step {
  if (!s) {
    return _defaultStep
  }

  const asNumber = parseInt(s, 10)
  if (!BookingWizard.steps.includes(asNumber)) {
    return _defaultStep
  } else {
    return asNumber
  }
}

@Controller()
export class TryItController {
  @Get(`/${TRY_IT_ROUTE}/__timestamp`)
  getWhenState(@Req() request: FastifyRequest): number | null {
    return request.session.wizard?.state?.when?.valueOf() || null
  }

  @Get(`/${TRY_IT_ROUTE}`)
  @Header('content-type', 'text/html')
  _tryIt(@Req() request: FastifyRequest, @Res() reply: FastifyReply) {
    this.tryIt('', request, reply)
  }

  @Get(`/${TRY_IT_ROUTE}/:step`)
  @Header('content-type', 'text/html')
  tryIt(
    @Param('step') _step,
    @Req() { session }: FastifyRequest,
    @Res() reply: FastifyReply
  ) {
    const step = parseStep(_step)
    const state = BookingWizard.fromSession(session)
    state.wizard = Wizards.goTo(step, state.wizard)
    if (state.wizard.currentStep !== step) {
      // TODO uncomment if you want to test the UI styles
      // state.wizard.currentStep = step
      // reply.send(TryItPage(state).toString())

      // TODO comment if you want to test the UI styles
      reply.redirect(303, `/${TRY_IT_ROUTE}/${state.wizard.currentStep}`)
    } else {
      reply.send(TryItPage(state).toString())
    }
  }

  @Post(`/${TRY_IT_ROUTE}/:step`)
  async submitTryItForm(
    @Param('step') _step,
    @Req() { session }: FastifyRequest,
    @Res() reply: FastifyReply,
    @Body() _input
  ) {
    const step = parseStep(_step)
    let state = BookingWizard.fromSession(session)
    state.wizard = Wizards.goTo(step, state.wizard)
    const input = _parseBookingForm(_input)
    const { wizardResult, shouldFetchPaymentIntent } = BookingWizard.goToNext(
      state,
      input
    )

    if ('error' in wizardResult) {
      throw wizardResult.error
    } else if ('result' in wizardResult) {
      // TODO decide what to do here
      session.wizard = undefined
      session.payment = undefined
      reply.redirect(303, '/')
    } else {
      // stateTransitionResult contains the next state
      state.wizard = wizardResult
      session.wizard = state.wizard

      let clientSecret
      if (shouldFetchPaymentIntent) {
        clientSecret = await fetchPaymentIntentForBooking(
          wizardResult.state.headCount
        )
        state = BookingWizard.paymentIntentReceived(state, clientSecret)
        session.wizard = state.wizard
        session.payment = state.payment
      }

      reply.redirect(303, `/${TRY_IT_ROUTE}/${wizardResult.currentStep}`)
    }
  }
}
