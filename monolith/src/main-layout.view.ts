import { html } from './web/templates'
import { MENU_ROUTE } from './menu.view'
import { TRY_IT_ROUTE } from './try-it-views'

function Header() {
  return html`
    <nav aria-label="Main Navigation" class="w-full bg-black text-white py-3">
      <ul class="flex flex-row items-center relative h-full space-x-6">
        <li>
          <a class="text-4xl" href="/">Gomë</a>
        </li>
        <li>
          <a class="text-xl" href="/${MENU_ROUTE}">Menu</a>
        </li>
        <li>
          <a class="text-xl" href="/${TRY_IT_ROUTE}">Try It</a>
        </li>
      </ul>
    </nav>
  `
}

function Footer() {
  return html`
    <section class="w-full py-8 text-lg bg-black text-white text-center">
      <span>info.gome@gmail.com | 12807 Pinnacle Dr, Germantown MD</span>
    </section>
  `
}

type scriptSpec =
  | string
  | {
      src: string
      async: true
    }
  | {
      src: string
      deferd: true
    }

export function MainLayout(
  {
    stylesheets: { internal = [], external = [] } = {},
    scripts = [],
  }: {
    stylesheets?: {
      internal?: string[]
      external?: string[]
    }
    scripts?: scriptSpec[]
  },
  main
): ReturnType<typeof html> {
  return html`
    <!DOCTYPE html>
    <html lang="en">
      <head>
        <link rel="icon" href="/favicon.ico" />
        <meta name="theme-color" content="#000000" />
        <meta name="description" content="In home chef service" />
        <link rel="manifest" href="/manifest.json" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta charset="UTF-8" />
        <title>Gomë</title>

        ${external.map((s) => html`<link rel="stylesheet" href="${s}" />`)}

        <link rel="stylesheet" href="/stylesheets/main.css" />

        ${internal.map(
          (s) => html`<link rel="stylesheet" href="/stylesheets/${s}" />`
        )}
        ${scripts.map((s) =>
          typeof s === 'string'
            ? html`<script src="${s}"></script>`
            : s && s['async']
            ? html`<script src="${s.src}" async></script>`
            : html`<script src="${s.src}" defer></script>`
        )}
      </head>
      <body>
        <header>${Header()}</header>
        <main>${main}</main>
        <footer>${Footer()}</footer>
      </body>
    </html>
  `
}
