import { Controller, Get, Header, Param, Req } from '@nestjs/common'
import { Menu, Cuisine } from '@gome/booking'
import { HomePage } from './homepage.view'
import { MenuPage, MENU_ROUTE } from './menu.view'

@Controller()
export class AppController {
  constructor() {}

  @Get()
  @Header('content-type', 'text/html')
  home(): string {
    return HomePage().toString()
  }

  @Get(`/${MENU_ROUTE}`)
  @Header('content-type', 'text/html')
  _menu() {
    return this.menu('')
  }

  @Get(`/${MENU_ROUTE}/:cuisine`)
  @Header('content-type', 'text/html')
  menu(@Param('cuisine') _cuisine): string {
    const cuisine: Cuisine.T = Cuisine.values.includes(_cuisine)
      ? _cuisine
      : 'french'
    return MenuPage(cuisine, Menu.menu[cuisine]).toString()
  }
}
