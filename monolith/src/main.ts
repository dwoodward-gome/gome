import { NestFactory } from '@nestjs/core'
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify'
import { join } from 'path'
import pino from 'pino'
import fastifySession from 'fastify-session'
import fastifyCookie from 'fastify-cookie'

import { AppModule } from './app.module'

const isProd = process.env.NODE_ENV === 'production'

async function bootstrap() {
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter({
      logger: pino({
        prettyPrint: isProd
          ? false
          : {
              colorize: true,
              messageFormat:
                '{levelLabel} {reqId} {req.method}{req.url}{res.statusCode}',
              ignore: 'level,time,pid,hostname,req,res,responseTime,reqId',
            },
      }),
    })
  )

  if (!isProd) {
    app.useStaticAssets({
      root: join(__dirname, '..', 'public'),
    })
  }

  app.register(fastifyCookie)
  app.register(fastifySession, {
    secret: 'secretsecretsecretsecretsecretsecret',
    cookieName: 'session_id',
    saveUninitialized: true,
    cookie: {
      httpOnly: true,
      secure: false,
    },
  })

  await app.listen(9000, '0.0.0.0')
}
bootstrap()
