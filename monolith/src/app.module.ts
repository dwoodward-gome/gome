import { Module } from '@nestjs/common'
import { AppController } from './app.controller'
import { HealthCheckController } from './health-check.controller'
import { TryItController } from './try-it.controller'

@Module({
  imports: [],
  controllers: [AppController, HealthCheckController, TryItController],
  providers: [],
})
export class AppModule {}
