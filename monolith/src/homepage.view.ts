import { html } from './web/templates'
import { MainLayout } from './main-layout.view'
import { TRY_IT_ROUTE } from './try-it-views'

function Card({ heading, body }) {
  return html`
    <section class="text-center space-y-4" style="max-width: 390px;">
      <h3 class="text-3xl">${heading}</h3>
      <p class="text-xl">${body}</p>
    </section>
  `
}

export function HomePage(): ReturnType<typeof MainLayout> {
  return MainLayout(
    {},
    html`
      <section
        class="flex flex-row justify-around items-center flex-wrap py-8 sm:py-12 space-y-5 sm:flex-row-reverse"
        style="background-color: rgb(230, 230, 183);"
      >
        <img
          src="/StandIn-Optimized.jpg"
          alt="Fresh food being prepared by a chef"
          width="710px"
          height="560px"
        />
        <section
          class="flex flex-col justify-center items-center space-y-6"
          style="min-width: 300px;"
        >
          <h1 class="text-5xl font-bold">Tired of soggy meal prep kits?</h1>
          <p class="text-2xl">
            We bring a chef to you for any ocassion, starting at $70 per plate.
          </p>
          <a class="p-4 text-2xl bg-black text-white" href="/${TRY_IT_ROUTE}"
            >Get started</a
          >
        </section>
      </section>
      <section
        class="flex flex-col justify-center items-center py-10 sm:py-16 space-y-10 xl:space-y-20"
      >
        <h2 class="text-center text-5xl font-semibold">How it works</h2>
        <section
          class="flex flex-col xl:flex-row items-center xl:justify-around w-full space-y-8 xl:space-y-0"
        >
          ${Card({
            heading: 'Customize your meal',
            body:
              "Whether it's calories, or dietary restrictions our chefs have you covered.",
          })}
          ${Card({
            heading: 'Book your date',
            body: 'Our chef will arrive exactly when and how you specify.',
          })}
          ${Card({
            heading: 'No mess, no fuss',
            body: 'Our chefs will handle all preparation and cleanup.',
          })}
        </section>
        <a class="p-4 text-2xl bg-black text-white" href="/${TRY_IT_ROUTE}"
          >Get started</a
        >
      </section>
    `
  )
}
