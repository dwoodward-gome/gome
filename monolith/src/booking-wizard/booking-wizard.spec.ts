import { make, Step, steps } from './booking-wizard'
import { goTo } from '../web/wizards'

describe('Wizards.goTo', () => {
  it('Should not allow a fresh flow to skip steps', () => {
    const flow = make()
    const actual = goTo(steps[1], flow).currentStep
    expect(actual).toEqual(steps[0])
  })

  it('Should not allow a fresh flow to skip to the last step', () => {
    const flow = make()
    const actual = goTo(steps[steps.length - 1], flow).currentStep
    expect(actual).toEqual(steps[0])
  })
})

describe('steps', () => {
  it('Should include all steps', () => {
    expect(steps.length).toEqual(Object.keys(Step).length / 2)
  })
})
