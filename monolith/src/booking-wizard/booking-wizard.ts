import { Session } from 'fastify'
import { partial, StructError, validate } from 'superstruct'
import { Booking } from '@gome/booking'
import * as Wizards from '../web/wizards'

// UTIL

export enum Step {
  RESERVATION,
  DETAILS,
  CHECKOUT,
}

export type T = {
  wizard: Wizards.T<Step, Partial<Booking.T>>
  payment: {
    clientSecret?: string
  }
}

export const steps = [Step.RESERVATION, Step.DETAILS, Step.CHECKOUT]

export function stepLt(s1: Step, s2: Step): boolean {
  const _s1 = steps.indexOf(s1)
  const _s2 = steps.indexOf(s2)
  return _s1 < _s2
}

// CONSTRUCTORS

export function make(): T {
  return {
    wizard: Wizards.make({ steps, state: Booking.make() }),
    payment: {},
  }
}

export function fromSession(session: Session): T {
  if (session.wizard) {
    // TODO stop assuming this cast will work when using a session store that
    // requires serialization/deserialization
    return <T>{
      wizard: session.wizard,
      payment: session.payment || {}, // TODO make sure this line makes sense
    }
  } else {
    return make()
  }
}

const schemasByStep = {
  [Step.RESERVATION]: partial(Booking.schemaFromHtml),
  [Step.DETAILS]: partial(Booking.schemaFromHtml),
  [Step.CHECKOUT]: Booking.schemaFromHtml,
}

function validateState(
  step: T['wizard']['currentStep'],
  state: T['wizard']['state']
) {
  const schema = schemasByStep[step]
  const [errors] = validate(state, schema)
  return errors
}

// PUBLIC FUNCTIONS

export function goToNext(
  currentState: T,
  newWizardState: unknown
): {
  wizardResult: Wizards.NextResult<Step, T['wizard']['state'], StructError>
  shouldFetchPaymentIntent?: boolean
} {
  const { wizard } = currentState

  // NOTE this copying fiasco is essentially a merge
  // previously undefined properties in newState were overriding defined ones
  // in currentState
  // TODO maybe come up with a cleaner way to do it...
  const _ = {
    ...wizard.state,
  }
  for (let key in <object>newWizardState) {
    if (newWizardState[key] !== undefined) {
      _[key] = newWizardState[key]
    }
  }

  const shouldFetchPaymentIntent =
    _.headCount > 0 && _.headCount !== wizard.state.headCount

  return {
    wizardResult: Wizards.goToNext(validateState, wizard, _),
    shouldFetchPaymentIntent,
  }
}

export function paymentIntentReceived(state: T, clientSecret: string): T {
  return {
    wizard: state.wizard,
    payment: {
      ...state.payment,
      clientSecret,
    },
  }
}
