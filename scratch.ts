type MainDishOrder = {}
type SideDishOrder = {}
type DessertOrder = {}
type Name = {}

type GuestUser = {
  provideName: Name
  selectMainDish: MainDishOrder
  selectSideDish: SideDishOrder
  selectDesert: DessertOrder
}

type Order = {}
type OwningUser = {
  setAllFormValuesExceptForIndividualUsersDishes: Order
  // all the actions the GuestUser can perform, the own can perform for themselves
}

/*
 * Todo
 * [ ] If logged in show link
 *      [ ] on click create a group order on the backend
 *      [ ] give them a modal with a link where they can share
 * [ ] Create a page and link to the guest UI page
 *      [ ] allow them to order (singularly) and update it on the backend
 *      [ ] have a default screen of some kind that shows them their order
 * [ ] Determine a way for the host to interact with the menu that makes sense
 */
